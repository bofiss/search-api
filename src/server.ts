import express from 'express';
import http from 'http';
import {applyMiddleware, applyRoutes } from './utils';
import middleware from './middleware';
import routes from './services';
import errorHandlers from './middleware/errorHandlers';
import dotenv from "dotenv";
dotenv.config();

// Handling uncaughtException and unhandledRejection
process.on("uncaughtException", e => {
  console.log(e);
  process.exit(1)
})

// unhandledRejection
process.on("unhandledRejection", e => {
  console.log(e);
  process.exit(1)
})

const router = express();
applyMiddleware(middleware, router);
applyRoutes(routes, router);
applyMiddleware(errorHandlers, router);
const {PORT = 3000} = process.env;
const server = http.createServer(router);


server.listen(PORT, () => console.log(`Server is running on port ${PORT}`));
